import React, { Component } from 'react';
import { SERVER_URL } from "../constants.js";
import OrderList from './OrderList';
import '../styles.css';
import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            isAuthenticated: false
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    login = (event) => {
        event.preventDefault();
        const user = {
            username: this.state.username,
            password: this.state.password
        };

        axios
            .post(SERVER_URL + "login", user)
            .then(res => {
                console.log("login successful");
                const jwtToken = res.headers.authorization;
                if (jwtToken !== null) {
                    sessionStorage.setItem("jwt", jwtToken);
                    this.setState({ isAuthenticated: true });
                }
            })
            .catch(err => console.error(err));
    };



    render() {
        if (this.state.isAuthenticated === true) {
            return <OrderList />;
        } else {
            return (
                <div className="container">
                <form onSubmit={this.login}>
                    <h1>simple-wms-react - Login</h1>
                    Benutzername:<br />
                    <input type="text" name="username"
                        onChange={this.handleChange}>
                    </input><br />
                    Passwort:<br />
                    <input type="password" name="password"
                        onChange={this.handleChange}>
                    </input><br />
                    <input type="submit" value="Anmeldung" />
                </form>
                </div>
            );
        }
    }

}

export default Login;