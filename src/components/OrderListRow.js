import React, {Component} from 'react';
import '../styles.css';

class OrderListRow extends Component {

    render() {
        return (
            <div className="orderrow">
                <p>{this.props.order.orderName}<br />Status: {this.props.order.orderStatus}</p>
                <button>Details</button>
            </div>
        );
    }
}

export default OrderListRow;