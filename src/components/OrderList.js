import React, { Component } from 'react';
import { SERVER_URL } from "../constants.js";
import axios from "axios";
import OrderListRow from './OrderListRow';
import OrderSearchBar from './OrderSearchBar';
import '../styles.css';

class OrderList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        }
    }

    componentDidMount() {
        console.log("componentDidMount");
        const token = sessionStorage.getItem("jwt");
        axios
            .get(SERVER_URL + "api/orders", {
                headers: { 'Authorization': token }
            })
            .then(response => {
                console.log(response.data);
                this.setState((prevState, props) => ({
                    orders: response.data
                }));
            })
            .catch(error => console.log(error));
    }

    render() {
        console.log("render");
        console.log(this.state.orders);
        const rows = this.state.orders.map(picking_order => (<OrderListRow key={picking_order.id} order={picking_order} />));
        return (
            <div className="container">
                <div className="header">
                    <h2>Kommissionieraufträge</h2>
                </div>
                <OrderSearchBar />
                <div className="list">
                    {rows}
                </div>
            </div>
        );
    }

}


export default OrderList;