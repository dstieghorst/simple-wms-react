import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import Login from './components/Login';

ReactDOM.render(<Login />, document.getElementById('root'));
